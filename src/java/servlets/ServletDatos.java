/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import clases.Datos;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author Amelia
 */
public class ServletDatos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Ejemplo Servlets</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Pruebas de Servlet </h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);        
        
        Datos objetoDatos = new Datos();
        if (request.getParameter("nombre")!= null)
        objetoDatos.setNombre(request.getParameter("nombre"));
        
        if (request.getParameter("apellido")!= null)
        objetoDatos.setApellido(request.getParameter("apellido"));
        
        if (request.getParameter("matematica")!= null)
        objetoDatos.setMatematica(request.getParameter("matematica"));
        
        if (request.getParameter("fisica")!= null)
        objetoDatos.setFisica(request.getParameter("fisica"));
        
        if (request.getParameter("quimica")!= null)
        objetoDatos.setQuimica(request.getParameter("quimica"));
        
        if (request.getParameter("genero")!= null)
        objetoDatos.setGenero(request.getParameter("genero"));
                
        // Set response content type
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
	String title = "Ejemplo Servlets";
        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " +
                "transitional//en\">\n";
        out.println(docType +
                "<html>\n" 
                + "<head><title>" + title + "</title></head>\n" 
                + "<body bgcolor=\"#f0f0f0\">\n" 
                + "<h1 align=\"center\">" + title + "</h1>\n" 
                + "<ul>\n" 
                + "  <li><b>Nombre</b>: " + objetoDatos.getNombre() + "\n" 
                + "  <li><b>Apellido</b>: " + objetoDatos.getApellido() + "\n" 
                + "  <li><b>Genero</b>: " + objetoDatos.getGenero() + "\n" 
                + "</ul>\n" 
                
                +"<table>" 
                + "<tr><td><b>CURSO</b></td>"
                + "    <td><b>ESTADO</b></td>"              
                + "</tr>"
                + "<tr><td><b>Matemática : </b></td>"
                + "    <td>"+objetoDatos.getMatematica()+"</td>"
                + "</tr>"
                + "<tr><td><b>Física : </b></td>"
                + "    <td>"+objetoDatos.getFisica()+ "</td>"
                + "</tr>"
                + "<tr><td><b>Química : </b></td>"
                + "    <td>"+objetoDatos.getQuimica()+ "</td>"
                + "</tr>"                
                +"</table>" 
                +  "</body></html>");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
