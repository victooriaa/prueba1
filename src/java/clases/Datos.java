/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Amelia
 */
public class Datos {
    private String nombre;
    private String apellido;
    private String matematica;
    private String fisica;
    private String quimica;
    private String genero;

    public Datos() {
        this.nombre="";
        this.apellido="";
        this.matematica="";
        this.fisica="";
        this.quimica="";
    }
        
    public String getGenero() {        
        if (genero.equals("M"))
            return "Masculino";
        else
            return "Femenino";
    }
    
    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getMatematica() {         
        if (matematica.compareTo("on")==0)
            return "Asignado";
        else
            return "NoAsignado";        
        
    }

    public void setMatematica(String matematica) {
        this.matematica = matematica;
    }

    public String getFisica() {
        if (fisica.compareTo("on")==0)
            return "Asignado";
        else
            return "NoAsignado";
    }

    public void setFisica(String fisica) {
        this.fisica = fisica;
    }

    public String getQuimica() {
        if (quimica.compareTo("on")==0)
            return "Asignado";
        else
            return "NoAsignado";
    }

    public void setQuimica(String quimica) {
        this.quimica = quimica;
    }    
}
